# Gitlab Work-Shop @ Test Crunch 2019

- Petr Vecera, [profiq](https://profiq.com)  
- [LinkedIn](https://www.linkedin.com/in/petr-ve%C4%8De%C5%99a-56274995/)


### Getting Started / prerequisite:
- Have Gitlab.com account, register here https://gitlab.com/users/sign_in


### Intro:
- We will be working with this sample project
- We will use technology NodeJS / Git / Docker / Linux
- **You don't need to know any of these technologies!!**
- We will work mainly in the browser, *if you want you can clone the 
project to your computer / work from your favorite IDE and install NodeJS
to see the commands on your computer.*


#### The project overview
- JavaScript NodeJS express server (web server)
- See package.json part "scripts" for available commands
- Default port and url is `localhost:3000`
- Start the project using command `npm start`
- Run any of the other commands using `npm run <name_of_the_script>`
from package.json for example `npm run test1`


### Step 1 - Fork the project / get familiar with Gitlab interface:
- Go to https://gitlab.com/profiq/gitlab-ci-workshop , click on FORK 
- From now on work from your own fork https://gitlab.com/<your_username>/gitlab-ci-workshop
 
### Step 2 - Getting started with Gitlab CI jobs
- https://docs.gitlab.com/ee/ci/
- Create file called `.gitlab-ci.yml` 
- Add first script: (https://docs.gitlab.com/ee/ci/yaml/README.html)

```
stages:
    - first_stage
    
First_job:
    stage: first_stage
    script:
        - echo "Hello world"
```
- In the left menu CI/CD -> Pipelines
- In the left menu CI/CD -> Jobs
- Settings -> CI/DI -> Runners


### Step 3 - Correct docker image / project job
- Set the right image https://docs.gitlab.com/ee/ci/yaml/README.html#image
- https://hub.docker.com/
- Run the ESLint check 

```
image: node:lts

stages:
    - first_stage
    
EsLint verification:
    stage: first_stage
    before_script:
        - npm install
    script:
        - npm run eslint
```

### Step 4 - Add second job / job artifacts
- Artifacts https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts
- Predefined variables https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

```
Test:
  stage: test
  before_script:
    - npm install
  script:
    - npm run test
  artifacts:
    name: 'test-report.${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}'
    paths:
      - mochawesome-report
    expire_in: 30 days
```

### Step 5 - All steps are separate jobs
- Each step runs at it's own, sharing only global settings
- Try to remove the `npm install` from the second step
```
Test:
  stage: test
  script:
    - npm run test
  artifacts:
    name: 'test-report.${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}'
    paths:
      - mochawesome-report
    expire_in: 30 days
```

### Step 6 - Cache files between the steps
- https://docs.gitlab.com/ee/ci/yaml/README.html#cache
- https://docs.gitlab.com/ee/ci/caching/index.html
- https://docs.gitlab.com/ee/ci/caching/index.html#good-caching-practices
 
 ```
image: node:lts

cache:
  paths:
    - node_modules/

stages:
    - first_stage
...
 ```

**Even with cache I recommend to have the necessary steps in place for the job to
be able to run on it's own.**
- Add the before `npm install` to the second step
 
 ### Step 7 - Multiple jobs in one stage
 - Speedups the pipeline execution
 - Split your workload into multiple runners 
 ```
Test1:
  stage: test
  before_script:
        - npm install
  script:
    - npm run test1

Test2:
  stage: test
  before_script:
    - npm install
  script:
    - npm run test2
```
 - https://docs.gitlab.com/ee/ci/yaml/README.html#extends
 
 ```
.test.base:
  stage: test
  before_script:
    - npm install

Test1:
  extends: .test.base
  script:
    - npm run test1

Test2:
  extends: .test.base
  script:
    - npm run test2
```
 
 
 ## PAUSE WAIT FOR EVERYONE
 
 ### Step 8 - Secret Variables / variables / when
- Add variables steps / with curl which checks the server
```
Variables-test:
  stage: first_stage  
  before_script:
    - npm install
  script:
    - node bin/www &
    - sleep 3
    - curl localhost:3000  
```
- Add secret variable SETTINGS -> CI/CD -> VARIABLES
```
Variables-test:
  stage: first_stage  
  before_script:
    - npm install
  script:
    - node bin/www &
    - sleep 3
    - curl localhost:3000    
    - node verify-api.js "kofola"
```
 
- Add regular variable

```
Variables-test:
  stage: first_stage  
  before_script:
    - npm install
  script:
    - node bin/www &
    - sleep 3
    - curl localhost:3000
    - node verify-customer.js "profiq"
    - node verify-api.js "kofola"
  variables:
    CUSTOMER: "profiq"
 ```
- https://docs.gitlab.com/ee/ci/yaml/README.html#when
- When:
```
Variables-test:
  stage: first_stage
  when: manual
  before_script:
    - npm install
  script:
    - node bin/www &
    - sleep 3
    - curl localhost:3000
    - node verify-customer.js "profiq"
    - node verify-api.js "kofola"
  variables:
    CUSTOMER: "profiq"
```
- Run manual VARIABLES
------------------------

### Step 9 - Run on Merge requests
- By default runs on every merge request 
- https://docs.gitlab.com/ee/ci/yaml/README.html#onlyexcept-basic
- Only merge requests

```
.test.base:
  stage: test
  only:
    - merge_requests
  before_script:
    - npm install

```


### Step 10 - Installing complex dependencies

```
Selenium:
  stage: first_stage
  before_script:
    # Install Java, unzip, python3
    - apt-get update
    - apt-get install -y default-jdk unzip
    - apt-get install -y python3 python3-pip
    - pip3 install selenium
    # Install chrome
    - wget --quiet https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    - dpkg -i google-chrome-stable_current_amd64.deb || apt-get -qq install -y -f
    - dpkg -i google-chrome-stable_current_amd64.deb
    # Install chromedriver
    - wget https://chromedriver.storage.googleapis.com/77.0.3865.40/chromedriver_linux64.zip
    - unzip chromedriver_linux64.zip
    - mv chromedriver /usr/bin/chromedriver
    - chmod +x /usr/bin/chromedriver
  script:
    - cd python-tests
    - python3 test.py
``` 

Or we can use docker image which already has all the dependencies installed.






